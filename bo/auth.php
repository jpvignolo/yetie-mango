<?php
header('Access-Control-Allow-Origin: *');
if (isset($_GET['password'])) {
	if (password_verify($_GET['password'],'$2y$10$V/YJQ8.Icox0nKiznk2lL..3Bsc3LrrmjDfy2brmSc5592Chqa6tO')) {
		echo '{"token": "$2y$10$V/YJQ8.Icox0nKiznk2lL..3Bsc3LrrmjDfy2brmSc5592Chqa6tO"}';
	} else {
		http_response_code(403);
	}
} else {
	http_response_code(403);
}
?>
