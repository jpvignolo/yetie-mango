<?php
require_once("firebaseautoloader.php");
require_once("src/firebase.php");

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
$serviceAccount = ServiceAccount::fromJsonFile('google-services.json');
