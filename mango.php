<?php
/*require_once 'Psr/Log/LogLevel.php';
require_once 'Psr/Log/LoggerInterface.php';
require_once 'Psr/Log/AbstractLogger.php';
require_once 'Psr/Log/NullLogger.php';
require_once 'MangoPay/Autoloader.php';*/
header("Access-Control-Allow-Origin: *");
require 'vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/project-5409796454043349927-firebase-adminsdk-jbpc1-257777f0b8.json');

$firebase = (new Factory)
	->withServiceAccount($serviceAccount)
	// The following line is optional if the project id in your credentials file
	// is identical to the subdomain of your Firebase project. If you need it,
	// make sure to replace the URL with the URL of your project.
	->withDatabaseUri('https://project-5409796454043349927.firebaseio.com')
	->create();
if (isset($_GET['env'])) {
	if ($_GET['env'] == 'sandbox' || $_GET['env'] == 'dev') {
		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/yetie-test-f0530ea2ec79.json');

		$firebase = (new Factory)
			->withServiceAccount($serviceAccount)
			// The following line is optional if the project id in your credentials file
			// is identical to the subdomain of your Firebase project. If you need it,
			// make sure to replace the URL with the URL of your project.
			->withDatabaseUri('https://yetie-test.firebaseio.com')
			->create();
	}
}
$database = $firebase->getDatabase();

$api = new MangoPay\MangoPayApi();
// sandbox

$isSandbox = false;
$api->Config->ClientId = 'yetieprod';
$api->Config->ClientPassword = 'gEAR24S7wwv8pgjvkQ5ba7My6EpmpwDjydAyUo7qPbQdR8qMmL';
$api->Config->TemporaryFolder = './mangotmp';
$api->Config->BaseUrl = 'https://api.mangopay.com';

if (isset($_GET['env'])) {
	if ($_GET['env'] == 'sandbox' || $_GET['env'] == 'dev') {
		$isSandbox = true;
		$api->Config->ClientId = 'yetie';
		$api->Config->ClientPassword = 'DROEW3NhFtVV06XsWvGwid3YiqgJipcHi3KEa6oVOPuAR73Hv2';
		$api->Config->TemporaryFolder = './mangotmp';
		$api->Config->BaseUrl = 'https://api.sandbox.mangopay.com';
	}
}

file_put_contents("mangotmp/log", "start \n", FILE_APPEND);
file_put_contents("php://stderr", "start ".$isSandbox." \n");

class MangoException extends Exception {
	public function __construct($message, $code = 0, Exception $previous = null) {
		parent::__construct(json_encode($message), $code, $previous);
	}

	/**
	 * Returns the json decoded message.
	 *
	 * @param bool $assoc
	 *
	 * @return mixed
	 */
	public function getDecodedMessage($assoc = false)
	{
		return json_decode($this->getMessage(), $assoc);
	}
}

function verifFields($json,$errorTab) {
	if (!isset($json->addr1))
		$errorTab['addr1'] = "addr1 missing";
	if (!isset($json->city))
		$errorTab['city'] = "city missing";
	if (!isset($json->postalcode))
		$errorTab['postalcode'] = "postalcode missiong";
	if (!isset($json->addr1))
		$errorTab['addr1'] = "addr1 missing";
	if (!isset($json->firstname))
		$errorTab['firstname'] = "firstname missing";
	if (!isset($json->lastname))
		$errorTab['lastname'] = "lastname missing";
	if (!isset($json->email))
		$errorTab['email'] = "email missing";
	if (!isset($json->birthday))
		$errorTab['birthday'] = "birthday missing";
	if (!isset($json->countryresidence))
		$errorTab['countryresidece'] = "countryresidence missing";
	if (!isset($json->countrybirth))
		$errorTab['countrybirth'] = "countrybirth missing";
	return $errorTab;
}

function createUserWallet($user, $json) {
	global $api;
	try {
		$Wallet = new \MangoPay\Wallet();
		$Wallet->Tag = "";
		$Wallet->Owners = array ($user->Id);
		$Wallet->Description = "Yetie Wallet";
		$Wallet->Currency = "EUR";

		$Result = $api->Wallets->Create($Wallet);
		return $Result;
	} catch(MangoPay\Libraries\ResponseException $e) {
		file_put_contents($api->Config->TemporaryFolder."/log", "createUserWallet ResponseException : ".$e->GetMessage()."\n", FILE_APPEND);
	} catch(MangoPay\Libraries\Exception $e) {
		file_put_contents($api->Config->TemporaryFolder."/log", "createUserWallet Exception : ".$e->getMessage()."\n", FILE_APPEND);
	}
}

function createUser($json, $type="SOLETRADER") {
	global $api;
	file_put_contents("php://stderr", "createUser json : ".print_r($json,true)."\n");
	file_put_contents($api->Config->TemporaryFolder."/log", "createUser json : ".print_r($json,true)."\n", FILE_APPEND);
	try {
		$error = false;
		$mandatoryRegion = false;
		$errorTab = array();
		if (preg_match('#\b(US|CA|MX)\b#i', ($json->countryresidence))) {
			$errorTab['region'] = "region missing";
		}
		$errorTab = verifFields($json,$errorTab);

		if (isset($json->addr1)) {
			if ($json->addr1 != "") {
				$soletraderAddress = new \MangoPay\Address();
				$soletraderAddress->AddressLine1 = $json->addr1;
				if (isset($json->addr2))
					$soletraderAddress->AddressLine2 = $json->addr2;
				$soletraderAddress->City = $json->city;
				if (isset($json->region))
					$soletraderAddress->Region	= $json->region;
				$soletraderAddress->PostalCode = $json->postalcode;
				$soletraderAddress->Country = ($json->countryresidence);
			}
		}

		$UserLegal = new \MangoPay\UserLegal();
		//$UserLegal->Tag = "custom meta";
		$UserLegal->HeadquartersAddress = $soletraderAddress;
		if (isset($json->type)) {
			$type = $json->type;
		}
		$countryresidence = "FR";
		if (isset($json->countryresidence)) {
			$countryresidence = $json->countryresidence;
		} else if (isset($json->countryresidance)) {
			$countryresidence = $json->countryresidance;
		}
		$countrybirth = "FR";
		if (isset($json->countrybirth)) {
			$countryresidence = $json->countrybirth;
		}
		$UserLegal->LegalPersonType = $type;
		$UserLegal->Name = $json->firstname." ".$json->lastname;
		if (isset($soletraderAddress))
			$UserLegal->LegalRepresentativeAddress = $soletraderAddress;
		$UserLegal->LegalRepresentativeBirthday = intval($json->birthday);
		$UserLegal->LegalRepresentativeCountryOfResidence = "FR";
		$UserLegal->LegalRepresentativeNationality = $countrybirth;
		$UserLegal->LegalRepresentativeEmail = $json->email;
		$UserLegal->LegalRepresentativeFirstName = $json->firstname;
		$UserLegal->LegalRepresentativeLastName = $json->lastname;
		$UserLegal->Email = $json->email;
		if (isset($json->companynumber))
			$UserLegal->CompanyNumber = $json->companynumber; // SIRET / SIREN ou truc du genre

		file_put_contents("php://stderr", "createUser UserLegal : ".json_encode($UserLegal)."\n");
		if (empty($errorTab)) {
			$ResultUser = $api->Users->Create($UserLegal);
			return $ResultUser;
		} else {
			throw new MangoException($errorTab, 1);
		}
	} catch (MangoPay\Libraries\ResponseException $e) {
		file_put_contents("php://stderr", "createUser start ResponseException : ".$e->GetMessage()." ".$e->GetErrorDetails()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "createUser ResponseException : ".$e->GetMessage()." ".$e->GetErrorDetails()."\n", FILE_APPEND);
	} catch(MangoPay\Libraries\Exception $e) {
		file_put_contents("php://stderr", "createUser start Exception : ".$e->GetMessage()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "createUser Exception : ".$e->GetMessage()."\n", FILE_APPEND);
	} catch (MangoException $e) {
		file_put_contents("php://stderr", "createUser start MangoException : ".$e->GetMessage()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "createUser MangoException : ".$e->GetMessage()."\n", FILE_APPEND);
	}
}

function createBankAccount($user, $json) {
	global $api;
	try {

		$accountAddress = new \MangoPay\Address();
		$accountAddress->AddressLine1 = $json->addr1;
		if (isset($json->addr2))
			$accountAddress->AddressLine2 = $json->addr2;
		$accountAddress->City = $json->city;
		if (isset($json->region))
			$accountAddress->Region	= $json->region;
		$accountAddress->PostalCode = $json->postalcode;
		$accountAddress->Country = $json->countryresidence;

		$BankAccount = new \MangoPay\BankAccount();
		$BankAccount->Type = "IBAN";
		$BankAccount->Details = new MangoPay\BankAccountDetailsIBAN();
		$BankAccount->Details->IBAN = $json->iban;
		if (isset($json->bic))
			$BankAccount->Details->BIC = $json->bic;
		$BankAccount->OwnerName = $json->firstname." ".$json->lastname;
		$BankAccount->OwnerAddress = $accountAddress;
		return $api->Users->CreateBankAccount($user->Id, $BankAccount);
	} catch(MangoPay\Libraries\ResponseException $e) {
		file_put_contents($api->Config->TemporaryFolder."/log", "createBankAccount ".$json->iban." ResponseException : ".$e->GetMessage()." ".$e->GetErrorDetails()."\n", FILE_APPEND);
	} catch(MangoPay\Libraries\Exception $e) {
		file_put_contents($api->Config->TemporaryFolder."/log", "createBankAccount ".$json->iban." Exception : ".$e->GetMessage()."\n", FILE_APPEND);
	}
}

function updateUser($json,$userId, $type = "SOLETRADER") {
	global $api;
	try {
		$error = false;
		$mandatoryRegion = false;
		$errorTab = array();
		if (preg_match('#\b(US|CA|MX)\b#i', $json->countryresidence)) {
			$errorTab['region'] = "region missing";
		}
		$errorTab = verifFields($json,$errorTab);

		$soletraderAddress = new \MangoPay\Address();
		$soletraderAddress->AddressLine1 = $json->addr1;
		if (isset($json->addr2))
			$soletraderAddress->AddressLine2 = $json->addr2;
		$soletraderAddress->City = $json->city;
		if (isset($json->region))
			$soletraderAddress->Region	= $json->region;
		$soletraderAddress->PostalCode = $json->postalcode;
		$soletraderAddress->Country = $json->countryresidence;

		$UserLegal = new \MangoPay\UserLegal();
		//$UserLegal->Tag = "custom meta";
		$UserLegal->HeadquartersAddress = $soletraderAddress;
		$UserLegal->LegalPersonType = $type;
		$UserLegal->Name = $json->firstname." ".$json->lastname;
		$UserLegal->LegalRepresentativeAddress = $soletraderAddress;
		$UserLegal->LegalRepresentativeBirthday = intval($json->birthday);
		$UserLegal->LegalRepresentativeCountryOfResidence = $json->countryresidence;
		$UserLegal->LegalRepresentativeNationality = $json->countrybirth;
		$UserLegal->LegalRepresentativeEmail = $json->email;
		$UserLegal->LegalRepresentativeFirstName = $json->firstname;
		$UserLegal->LegalRepresentativeLastName = $json->lastname;
		$UserLegal->Email = $json->email;
		$UserLegal->Id = $userId;
		if (isset($json->companynumber)) {
			file_put_contents($api->Config->TemporaryFolder."/log", "updateUser companynumber : ".$json->companynumber."\n", FILE_APPEND);
			$UserLegal->CompanyNumber = $json->companynumber; // SIRET / SIREN ou truc du genre
		}

		if (empty($errorTab)) {
			$ResultUser = $api->Users->Update($UserLegal);
			return $ResultUser;
		} else {
			throw new MangoException($errorTab, 1);
		}

	} catch (MangoPay\Libraries\ResponseException $e) {
		file_put_contents($api->Config->TemporaryFolder."/log", "updateUser ResponseException : ".$e->GetMessage()."\n", FILE_APPEND);
	} catch(MangoPay\Libraries\Exception $e) {
		file_put_contents($api->Config->TemporaryFolder."/log", "updateUser Exception : ".$e->GetMessage()."\n", FILE_APPEND);
	} catch (MangoException $e) {
		file_put_contents($api->Config->TemporaryFolder."/log", "updateUser MangoException : ".$e->GetMessage()."\n", FILE_APPEND);
	}
}

function preAuthorize($json) {
	global $api;
	$CardPreAuthorization = new \MangoPay\CardPreAuthorization();
	$CardPreAuthorization->Tag = "custom meta";
	$CardPreAuthorization->AuthorId = $json->userId;
	$CardPreAuthorization->DebitedFunds = new \MangoPay\Money();
	$CardPreAuthorization->DebitedFunds->Currency = "EUR";
	$CardPreAuthorization->DebitedFunds->Amount = $json->amount;
	$CardPreAuthorization->SecureMode = "DEFAULT";
	$CardPreAuthorization->CardId = $json->cardId;
	$CardPreAuthorization->SecureModeReturnURL = "http://www.yetie.fr/yetier-mango/info.php";

	$Result = $api->CardPreAuthorizations->Create($CardPreAuthorization);
		return $Result;
}


function createPage($userId, $KYCDocumentId, $file) {
	global $api;
	try {
		file_put_contents($api->Config->TemporaryFolder."/log", "createPage init $userId $KYCDocumentId ".$file['tmp_name']." \n", FILE_APPEND);
		file_put_contents($api->Config->TemporaryFolder."/log", "createPage file read \n", FILE_APPEND);
		file_put_contents($api->Config->TemporaryFolder."/log", "createPage KycPage object \n", FILE_APPEND);
		file_put_contents("php://stderr", "createPage init $userId $KYCDocumentId ".$file['tmp_name']." \n");
		file_put_contents("php://stderr", "createPage file read \n");
		file_put_contents("php://stderr", "createPage KycPage object \n");
		$Result = $api->Users->CreateKycPageFromFile($userId, $KYCDocumentId, $file['tmp_name']);
		file_put_contents($api->Config->TemporaryFolder."/log", "createPage result OK ".print_r($Result,true)." \n", FILE_APPEND);
	} catch(MangoPay\Libraries\ResponseException $e) {
		// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
		file_put_contents("php://stderr", "createPage ResponseException : ".$e->GetMessage()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "createPage ResponseException : ".$e->GetMessage()."\n", FILE_APPEND);
		return false;
	} catch(MangoPay\Libraries\Exception $e) {
		// handle/log the exception $e->GetMessage()
		file_put_contents("php://stderr", "createPage Exception : ".$e->GetMessage()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "createPage Exception : ".$e->GetMessage()."\n", FILE_APPEND);
		return false;
	}
	return true;
}


function submitKycDocument($userId, $KYCDocumentId) {
	//submitting kyc
	global $api;
	try {
		$KycDocument = new \MangoPay\KycDocument();
		$KycDocument->Status = "VALIDATION_ASKED";
		$KycDocument->Id = $KYCDocumentId;

		if (isset($_POST['yetie']))
			$KycDocument->Tag = $_POST['yetie'];

		$Result = $api->Users->UpdateKycDocument($userId, $KycDocument);
		file_put_contents("php://stderr", "submitKycDocument submitting kyc OK ".print_r($Result,true)." \n");
		file_put_contents($api->Config->TemporaryFolder."/log", "submitKycDocument submitting kyc OK ".print_r($Result,true)." \n", FILE_APPEND);
		return $Result;	
	} catch(MangoPay\Libraries\ResponseException $e) {
		// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
		file_put_contents("php://stderr", "submitKycDocument ResponseException : ".$e->GetMessage()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "submitKycDocument ResponseException : ".$e->GetMessage()."\n", FILE_APPEND);
	} catch(MangoPay\Libraries\Exception $e) {
		// handle/log the exception $e->GetMessage()
		file_put_contents("php://stderr", "submitKycDocument Exception : ".$e->GetMessage()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "submitKycDocument Exception : ".$e->GetMessage()."\n", FILE_APPEND);
	}
}

$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
file_put_contents($api->Config->TemporaryFolder."/log", "pathinfo ".print_r($_SERVER['PATH_INFO'],true)." \n", FILE_APPEND);

if ($request[0] == 'createuser') {
	$jsonstr = file_get_contents('php://input');
	$json = json_decode($jsonstr);
	file_put_contents($api->Config->TemporaryFolder."/log", "createuser init ".$jsonstr." \n", FILE_APPEND);
	file_put_contents("php://stderr", "createuser init ".$jsonstr." \n");
	$resultCreateUser = createUser($json);
	if (is_object($resultCreateUser)) {
		file_put_contents($api->Config->TemporaryFolder."/log", "createuser user created : .\n", FILE_APPEND);
		$resultCreateWallet = createUserWallet($resultCreateUser, $json);
		if (is_object($resultCreateWallet)) {
			file_put_contents($api->Config->TemporaryFolder."/log", "createuser walltet created : \n", FILE_APPEND);
			if (isset($json->iban)) {
				$resultCreateBankAccount = createBankAccount($resultCreateUser, $json);
			}
			if (is_object($resultCreateBankAccount))
				echo '{"idUser": '.$resultCreateUser->Id.',"idWallet": '.$resultCreateWallet->Id.',"idBankAccount": '.$resultCreateBankAccount->Id.'}';
			else
				echo '{"idUser": '.$resultCreateUser->Id.',"idWallet": '.$resultCreateWallet->Id.',"idBankAccount": 0}';
		} else {
			file_put_contents($api->Config->TemporaryFolder."/log", "createuser resultCreateWallet is not an object \n", FILE_APPEND);
		}
	} else {
		file_put_contents($api->Config->TemporaryFolder."/log", "createuser resultCreateUser is not an object \n", FILE_APPEND);
	}
} else if ($request[0] == 'updateuser') {
	$jsonstr = file_get_contents('php://input');
	$json = json_decode($jsonstr);
	$oldibanid = (int)$request[2];
	$ibanupdate = $request[3] === 'true' ? true : false;
	file_put_contents($api->Config->TemporaryFolder."/log", "updateuser init ".$jsonstr." \n", FILE_APPEND);
	$resultUpdateUser = updateuser($json,$request[1]);
	if (is_object($resultUpdateUser)) {
		file_put_contents($api->Config->TemporaryFolder."/log", "updateUser is object result : ".$ibanupdate."\n", FILE_APPEND);
		if ($ibanupdate) {
			if (strpos($json->iban, 'xxx') !== true) {
				file_put_contents($api->Config->TemporaryFolder."/log", "updateUser is object will create BankAccount \n", FILE_APPEND);
				$resultCreateBankAccount = createBankAccount($resultUpdateUser, $json);
			}
		}
		
		$resultstr = "";
		if (is_object($resultCreateBankAccount))
			echo $resultstr = '{"idUser": '.$resultUpdateUser->Id.',"idWallet": '.$json->walletid.',"idBankAccount": '.$resultCreateBankAccount->Id.'}';
		else
			echo $resultstr = '{"idUser": '.$resultUpdateUser->Id.',"idWallet": '.$json->walletid.',"idBankAccount": '.$oldibanid.'}';
		file_put_contents($api->Config->TemporaryFolder."/log", "updateUser result ".$resultstr."\n", FILE_APPEND);
	} else {
		file_put_contents($api->Config->TemporaryFolder."/log", "updateUser no object result\n", FILE_APPEND);
	}
} else if ($request[0] == 'upload') {
	file_put_contents($api->Config->TemporaryFolder."/log", "upload file ".print_r($_FILES,true)." type ".$_POST['kyc']." \n", FILE_APPEND);
	try {
		$KycDocument = new \MangoPay\KycDocument();
		if (isset($_POST['yetie']))
			$KycDocument->Tag = $_POST['yetie'];
		else {
			$KycDocument->Tag = "";
		}
		if (!strcasecmp($_POST['kyc'],"IDENTITY_PROOF")) {
			$KycDocument->Type = "IDENTITY_PROOF";
		} else if (!strcasecmp($_POST['kyc'],"REGISTRATION_PROOF")) {
			$KycDocument->Type = "REGISTRATION_PROOF";
		}
		file_put_contents($api->Config->TemporaryFolder."/log", "Mangoid ".$_POST['mangoid']." \n", FILE_APPEND);
		if ($_POST['mangoid'] == 0) {
			// WE should maybe create a mango user
		}

		$Result = $api->Users->CreateKycDocument($_POST['mangoid'], $KycDocument);
		file_put_contents($api->Config->TemporaryFolder."/log", "Result create kyc Document ".print_r($Result,true)." \n", FILE_APPEND);
		$res = true;
		foreach ($_FILES as $key => $file) {
			$tmpRes = createPage($_POST['mangoid'], $Result->Id, $file);
			file_put_contents($api->Config->TemporaryFolder."/log", "createPage result $tmpRes \n", FILE_APPEND);
			$res = $res && $tmpRes; 
		}
		file_put_contents($api->Config->TemporaryFolder."/log", "createPage result total $res \n", FILE_APPEND);
		if ($res) {
			$resSubmit = submitKycDocument($_POST['mangoid'], $Result->Id);
			if (is_object($resSubmit)) {
				file_put_contents($api->Config->TemporaryFolder."/log", "response {'kyc_id':".$resSubmit->Id.",'status':'".$resSubmit->Status."','type':'".$resSubmit->Type."'} \n", FILE_APPEND);
				echo "{'kyc_id':".$resSubmit->Id.",'status':'".$resSubmit->Status."','type':'".$resSubmit->Type."'}";
			}
		}
	} catch(MangoPay\Libraries\ResponseException $e) {
		// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
		file_put_contents($api->Config->TemporaryFolder."/log", "ResponseException kyc ".$e->GetCode()." ".$e->GetMessage()." ".$e->GetErrorDetails()." \n", FILE_APPEND);

	} catch(MangoPay\Libraries\Exception $e) {
		// handle/log the exception $e->GetMessage()
		file_put_contents($api->Config->TemporaryFolder."/log", "Exception kyc ".$e->GetMessage()." \n", FILE_APPEND);

	}

} else if (($request[0] == 'cardregistrations')) {
	file_put_contents($api->Config->TemporaryFolder."/log", "Card Registered start\n", FILE_APPEND);
	file_put_contents("php://stderr", "Card Registered start\n", FILE_APPEND);
	$jsonstr = file_get_contents('php://input');
	$json = json_decode($jsonstr);
	file_put_contents($api->Config->TemporaryFolder."/log", "Card Registered start jsonstr ".$jsonstr." \n", FILE_APPEND);
	file_put_contents($api->Config->TemporaryFolder."/log", "Card Registered start json ".print_r($json,true)." \n", FILE_APPEND);
	file_put_contents("php://stderr", "Card Registered start jsonstr ".$jsonstr." \n", FILE_APPEND);
	file_put_contents("php://stderr", "Card Registered start json ".print_r($json,true)." \n", FILE_APPEND);
	try {
		$CardRegistration = new \MangoPay\CardRegistration();
		$CardRegistration->Tag = "custom meta";
		$CardRegistration->UserId = $json->UserId;
		$CardRegistration->Currency = $json->Currency;
		$CardRegistration->CardType = $json->CardType;

		$Result = $api->CardRegistrations->Create($CardRegistration);
		if (is_object($Result)) {
			file_put_contents($api->Config->TemporaryFolder."/log", "Card Registered cardregistrations ".print_r($Result,true)." \n", FILE_APPEND);
			file_put_contents("php://stderr", "Card Registered cardregistrations ".print_r($Result,true)." \n", FILE_APPEND);
			echo '{"Id":"'.$Result->Id.'","AccessKey":"'.$Result->AccessKey.'","PreregistrationData":"'.$Result->PreregistrationData.'","CardRegistrationURL":"'.$Result->CardRegistrationURL.'"}';
		}
	} catch(MangoPay\Libraries\ResponseException $e) {
	// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
		file_put_contents($api->Config->TemporaryFolder."/log", "ResponseException cardregistrations ".$e->GetCode()." ".$e->GetMessage()." ".$e->GetErrorDetails()." \n", FILE_APPEND);
		file_put_contents("php://stderr", "ResponseException cardregistrations ".$e->GetCode()." ".$e->GetMessage()." ".$e->GetErrorDetails()." \n", FILE_APPEND);
	} catch(MangoPay\Libraries\Exception $e) {
	// handle/log the exception $e->GetMessage()
		file_put_contents($api->Config->TemporaryFolder."/log", "Exception cardregistrations ".$e->GetMessage()." \n", FILE_APPEND);
		file_put_contents("php://stderr", "Exception cardregistrations ".$e->GetMessage()." \n", FILE_APPEND);
	}
} else if ($request[0] == 'updatecardregistration') {
	file_put_contents("php://stderr", "updatecardregistration start ".$_GET['data']."\n");
	try {
		$CardRegistration = new \MangoPay\CardRegistration();
		$CardRegistration->Id = $request[1];
		$CardRegistration->RegistrationData = "data=".$_GET['data'];

		$Result = $api->CardRegistrations->Update($CardRegistration);
		if (is_object($Result)) {
			file_put_contents("php://stderr", "updatecardregistration ".$_GET['data'].' $request[1] '.$request[1]." result ".json_encode($Result)." \n");
			file_put_contents($api->Config->TemporaryFolder."/log", "updatecardregistration ".json_encode($Result).' $request[1] '.$request[1]." \n", FILE_APPEND);
			echo json_encode($Result);
		}

	} catch(MangoPay\Libraries\ResponseException $e) {
		// handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
		file_put_contents($api->Config->TemporaryFolder."/log", "ResponseException updatecardregistration ".$e->GetCode()." ".$e->GetMessage()." ".$e->GetErrorDetails()." \n", FILE_APPEND);
	} catch(MangoPay\Libraries\Exception $e) {
		// handle/log the exception $e->GetMessage()
		file_put_contents($api->Config->TemporaryFolder."/log", "Exception updatecardregistration ".$e->GetMessage()." \n", FILE_APPEND);
	}
} else if($request[0] == 'preauthorize') {
	file_put_contents("php://stderr", "preauthorize start\n");
	$jsonstr = file_get_contents('php://input');
	file_put_contents($api->Config->TemporaryFolder."/log", "preauthorize start ".$jsonstr." \n", FILE_APPEND);
	$json = json_decode($jsonstr);
	try {
		$Result = preauthorize($json);
		file_put_contents("php://stderr", "preauthorize ".json_encode($Result)."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "preauthorize ".json_encode($Result)." \n", FILE_APPEND);
		echo json_encode($Result);
	} catch(MangoPay\Libraries\ResponseException $e) {
		file_put_contents("php://stderr", "preauthorize ResponseException ".$e->GetCode()." ".$e->GetMessage()." ".$e->GetErrorDetails()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "ResponseException preauthorize ".$e->GetCode()." ".$e->GetMessage()." ".$e->GetErrorDetails()." \n", FILE_APPEND);

	} catch(MangoPay\Libraries\Exception $e) {
		file_put_contents("php://stderr", "preauthorize Exception ".$e->GetMessage()."\n");
		file_put_contents($api->Config->TemporaryFolder."/log", "Exception preauthorize ".$e->GetMessage()." \n", FILE_APPEND);

	}
}
else {
	file_put_contents($api->Config->TemporaryFolder."/log", "no valid REST method ".$request[0]." \n", FILE_APPEND);
}

?>
