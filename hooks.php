<?php
require 'vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/project-5409796454043349927-firebase-adminsdk-jbpc1-257777f0b8.json');

$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    // The following line is optional if the project id in your credentials file
    // is identical to the subdomain of your Firebase project. If you need it,
    // make sure to replace the URL with the URL of your project.
    ->withDatabaseUri('https://project-5409796454043349927.firebaseio.com')
    ->create();

$database = $firebase->getDatabase();

$TEMP_FOLDER = "./mangotmp/";
if (isset($_GET['EventType'])) {
	file_put_contents($TEMP_FOLDER."/hookslogs","EventType= ".$_GET['EventType']." , RessourceId=".$_GET['RessourceId']."\n", FILE_APPEND);
	$needAlert = false;
	if (stripos($_GET['EventType'],"kyc") !== false) {
		file_put_contents($TEMP_FOLDER."/hookslogs",'EventType '.$_GET['EventType']." is  kyc start users_kyc_updates/".$_GET['RessourceId']."\n", FILE_APPEND);
		if (stripos($_GET['EventType'],"SUCCEEDED") !== false || stripos($_GET['EventType'],"FAILED") !== false) {
			$needAlert = true;
		}
		file_put_contents($TEMP_FOLDER."/hookslogs",'EventType '.$_GET['EventType']." is  kyc second users_kyc_updates/".$_GET['RessourceId']."\n", FILE_APPEND);
		$updates = [
			'id' => intval($_GET['RessourceId']),
			'status' => str_ireplace("kyc_", "", $_GET['EventType']),
			'needAlert' => $needAlert
		];
		file_put_contents($TEMP_FOLDER."/hookslogs",'EventType '.$_GET['EventType']." is  kyc before getValue users_kyc_updates/".$_GET['RessourceId']."\n", FILE_APPEND);
		$snapshot = $database
			->getReference('users_kyc_updates/'.$_GET['RessourceId'])
			->getSnapshot();
		$val = $snapshot->getValue();

		file_put_contents($TEMP_FOLDER."/hookslogs",'users_kyc_updates/'.$_GET['RessourceId']." ".$val."\n", FILE_APPEND);
		$snapshot2 = $database
			->getReference('users/'.$val.'/kyc')
			->getSnapshot();
		foreach ($snapshot2->getValue() as $key => $value) {
			if ($value['id'] == $_GET['RessourceId']) {
				print_r(($value));
				$database->getReference('users/'.$val.'/kyc/'.$key)->update($updates);
				$snapshotToken = $database
					->getReference('users/'.$val.'/token')
					->getSnapshot();
				$token = $snapshotToken->getValue();
				
				if (stripos($_GET['EventType'],"FAILED") !== false) {
					$body_txt = "à été refusé. Veuillez l'envoyer à nouveau.";
					if (stripos($key, 'IDENTITY_PROOF') !== false) {
						$kyc_type_txt = "pièce d'identité";
						$title_txt = "refusée";
					} else if (stripos($key, 'REGISTRATION_PROOF') !== false) {
						$kyc_type_txt = "extrait de kbis";
						$title_txt = "refusé";
					}
					$database->getReference('/notifications/')->set(
						[$token => 
							['message' => ucfirst($kyc_type_txt)." ".$title_txt,
							 'body' => "Votre ".$kyc_type_txt." ".$body_txt]
						]
					);
				} else if (stripos($_GET['EventType'],"SUCCEEDED") !== false) {
					$snapshot3 = $database
						->getReference('users/'.$val.'/kyc')
						->getSnapshot();
					$okcount = 0;
					foreach ($snapshot3->getValue() as $key => $value) {
						if (stripos($value['status'],"SUCCEEDED") !== false) {
							$okcount++;
						}

					}
					if ($okcount == 2) {
						$database->getReference('/notifications/')->set(
							[$token => 
								['message' => "Vos documents d'identité ont été acceptés.",
								 'body' => "Vous pouvez désormais postuler a des missions près de chez vous."]
							]
					);
					}
				}


			}
		};

	} else {
		file_put_contents($TEMP_FOLDER."/hookslogs",'EventType '.$_GET['EventType']." not kyc\n", FILE_APPEND);
	}
} else {
	file_put_contents($TEMP_FOLDER."/hookslogs","No EventType set ".print_r($_GET,true)."\n", FILE_APPEND);
}
?>
