#!/bin/bash
if [ "$NODE_ENV" = "production" ]
then
	cp src/environments/environment.prod.ts src/environments/environment.ts
else
	cp src/environments/environment.dev.ts src/environments/environment.ts
fi

